vim-mkdbib
==========

*vim-mkdbib* aims to provide
[vim-pandoc](https://github.com/vim-pandoc/vim-pandoc)'s bibliographic
search functionality without the whole plugin (for users of other
implementations of markdown plugins for vim (like e.g. [plasticboy's
vim-markdown](https://github.com/plasticboy/vim-markdown)).


### Borrowings 

The whole thing is a copy of
[vim-pandoc](https://github.com/vim-pandoc/vim-pandoc).
