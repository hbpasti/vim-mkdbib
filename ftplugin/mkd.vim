" Autocomplete citationkeys using function
" ----------------------------------------
if has('python')
    call mkd#Mkd_Find_Bibfile()

    let s:completion_type = ""
    setlocal omnifunc=mkd#Mkd_Complete
endif

" Mappings
" --------

" Make the current line a setext-style H1 header
nmap <buffer> H= YpVr=<esc>
vmap <buffer> H= YpVr=<esc>
" Make the current line a setext-style H1 header
nmap <buffer> h- YpVr-<esc>
vmap <buffer> h- YpVr-<esc>
" Make the current line an atx-style h1 header
nmap <buffer> h1 1I#<esc>
vmap <buffer> h1 1I#<esc>
" Make the current line an atx-style h2 header
nmap <buffer> h2 2I#<esc>
vmap <buffer> h2 2I#<esc>
" Make the current line an atx-style h3 header
nmap <buffer> h3 3I#<esc>
vmap <buffer> h3 3I#<esc>
" Make the current line an atx-style h4 header
nmap <buffer> h4 4I#<esc>
vmap <buffer> h4 4I#<esc>
" Make the current line an atx-style h5 header
nmap <buffer> h5 5I#<esc>
vmap <buffer> h5 5I#<esc>
" Make the current line an atx-style h6 header
nmap <buffer> h6 6I#<esc>
vmap <buffer> h6 6I#<esc>
