"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Completion:

function! mkd#Mkd_Find_Bibfile()
    
    if !has('python')
        finish
    endif
    
python<<EOF
import vim
import os
from os.path import exists, splitext, expanduser, expandvars, isdir
from glob import glob
from subprocess import Popen, PIPE

bib_extensions = ["json", "ris", "mods", "biblates", "bib"]

if vim.current.buffer.name != None:
	file_name, ext = splitext(vim.current.buffer.name)

	# first, we check for files named after the current file in the current dir
	bibfiles = [f for f in glob(file_name + ".*") if splitext(f)[1] in bib_extensions]
else:
	bibfiles = []

# we search for any bibliography in the current dir
if bibfiles == []:
	bibfiles = [f for f in glob("*.*") if f.split(".")[-1] in bib_extensions]

# we search in mkd's local data dir
if bibfiles == []:
	b = ""
	if exists(expandvars("$HOME/.mkd/")):
		b = expandvars("$HOME/.mkd/")
	elif exists(expandvars("%APPDATA%/mkd/")):
		b = expandvars("%APPDATA%/mkd/")
	if b != "":
		bibfiles = [f for f in glob(b + "default.*") if f.split(".")[-1] in bib_extensions]

# we search for bibliographies in texmf
if bibfiles == [] and vim.eval("executable('kpsewhich')") != '0':
	texmf = Popen(["kpsewhich", "-var-value", "TEXMFHOME"], stdout=PIPE, stderr=PIPE).\
                communicate()[0].strip()
	if exists(texmf):
		bibfiles = [f for f in glob(texmf + "/*") if f.split(".")[-1] in bib_extensions]

# we append the items in g:mkd_bibfiles, if set
if vim.eval("exists('g:mkd_bibfiles')") != "0":
	bibfiles.extend(vim.eval("g:mkd_bibfiles"))

# we expand file paths, and
# check if the items in bibfiles are readable and not directories
if bibfiles != []:
	bibfiles = list(map(lambda f : expanduser(expandvars(f)), bibfiles))
	bibfiles = list(filter(lambda f : os.access(f, os.R_OK) and not isdir(f), bibfiles))

vim.command("let b:mkd_bibfiles = " + str(bibfiles))
EOF
endfunction

function! mkd#Mkd_Complete(findstart, base)
	if a:findstart
		" return the starting position of the word
		let line = getline('.')
		let pos = col('.') - 1
		while pos > 0 && line[pos - 1] !~ '\\\|{\|\[\|<\|\s\|@\|\^'
			let pos -= 1
		endwhile

		let line_start = line[:pos-1]
		if line_start =~ '.*@$'
			let s:completion_type = 'bib'
		else
			let s:completion_type = ''
		endif
		return pos
	else
		"return suggestions in an array
		let suggestions = []
		if s:completion_type == 'bib'
			" suggest BibTeX entries
			"let suggestions = mkd#Mkd_BibKey(a:base)
			let suggestions = mkd_bib#MkdBibSuggestions(a:base)
		endif
		return suggestions
	endif
endfunction

function! mkd#MkdContext()
    let curline = getline('.')
    if curline =~ '.*@[^ ;\],]*$'
		return "\<c-x>\<c-o>"
    endif
endfunction
